import { abilitiesPlugin } from '@casl/vue'

export default ({ Vue }) => {
  Vue.use(abilitiesPlugin)
}
