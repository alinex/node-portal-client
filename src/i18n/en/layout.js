export default {
  menu: {
    language: 'Language',
    settings: 'Settings',
    login: 'Login',
    loginSub: 'With your account',
    logout: 'Logout',
    profile: 'Profile'
  },
  sidebarHide: 'Hide or display left side menu',
  login: {
    title: 'Login',
    email: 'Email',
    emailError: 'We need your registered email',
    password: 'Password',
    passwordError: 'The password is mandatory',
    submit: 'Login',
    success: 'Successfully authenticated',
    fail: 'Could not login!'
  },
  logout: {
    success: 'Successfully logged out'
  },
  loader: {
    loading: 'Loading data...',
    sending: 'Waiting for server response...'
  },
  form: {
    close: 'Close',
    save: 'Save',
    saveSuccess: 'Changes to {type} śaved successfully',
    create: 'New',
    createSuccess: 'New {type} created successfully',
    delete: 'Delete',
    deleteSuccess: 'Deleted {type} successfully',
    autocomplete: 'Type some characters to search',
    duplicate: `Role "{role}" is already in list`,
    error: 'Could not send Form',
    errorDetail: 'Please review the red marked fields again.'
  },
  notFound: 'Sorry, nothing here...',
  goBack: 'Go Back'
}
