import layout from './layout'
import core from './core'
import policy from './policy'
import colab from './colab'

export default {
  // modules
  layout,
  core,
  policy,
  colab,
  // general parts
  title: 'Administration Portal',
  home: {
    title: 'Home',
    subtitle: 'Dashboard'
  }
}
