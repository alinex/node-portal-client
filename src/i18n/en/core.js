export default {
  title: 'System Administration',
  info: {
    title: 'Info',
    subtitle: 'System Information',
    about1: 'The administration panel is an easy and fast way to manage IT systems for technical and non technical staff.',
    about2: 'It is a universal control interface which can become real powerful by adding modules for special tasks. This modules will interact with the real systems in a proper and safe way. The user has no direct control but the limited functionality like defined in the module.'
  },
  users: {
    title: 'Users',
    type: 'user',
    subtitle: 'Account Administration',
    list: 'No users | One user | {count} users',
    detail: 'Detail',
    createButton: 'Add new User',
    formLogin: 'Login Data',
    formLoginDesc: 'The current password will not be shown, set it if you want to change it.',
    email: {
      title: 'Email',
      error: 'A valid email address is needed to login'
    },
    password: {
      title: 'Password',
      titleNew: 'New Password',
      error: 'The password should be two times exactly the same with at least 6 characters',
      retype: 'retype password to confirm'
    },
    formPersonal: 'Personal Information',
    formPersonalDesc: 'The Gravatar service from WordPress is used to get the avatar icon linked to your email address.',
    avatar: {
      title: 'Avatar',
      link: 'Link to WordPress to change Gravatar image'
    },
    nickname: {
      title: 'Nickname',
      error: 'The nickname should have at least 4 characters'
    },
    name: {
      title: 'Full Name'
    },
    position: {
      title: 'Position',
      error: 'The position should have at least 3 characters'
    },
    formAccess: 'Access Control',
    disabled: {
      title: 'Disabled',
      desc: 'user will no longer be able to login'
    }
  },
  roles: {
    title: 'Roles',
    type: 'role',
    subtitle: 'Access Rights',
    list: 'No roles | One role | {count} roles',
    detail: 'Detail',
    createButton: 'Add new Role',
    formMeta: 'Meta Data',
    formMetaDesc: 'Descriptive information of role.',
    name: {
      title: 'Name',
      error: 'The name is needed as identifier to select the role.'
    },
    description: {
      title: 'Description'
    },
    disabled: {
      title: 'Disabled',
      desc: 'role will currently not be used'
    },
    formRights: 'Access Rights',
    formRightsDesc: 'The rules defining all abilities for this rule.',
    inverted: {
      title: 'Type',
      desc: 'disallow access instead of allow it'
    },
    actions: {
      title: 'Action'
    },
    subject: {
      title: 'Subject'
    },
    conditions: {
      title: 'Conditions'
    },
    fields: {
      title: 'Fields'
    }
  }
}
